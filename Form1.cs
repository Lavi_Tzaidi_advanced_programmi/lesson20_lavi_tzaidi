﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Net.Sockets;
/*
 
     For some reason my comunicaton between the server and the client
     didnt work and it messed every thing up 
     all the function are working excpet from the talking between them
    
     */



namespace Lesson20
{
    public partial class Form1 : Form
    {
        public Form1()
        {

            InitializeComponent();
        }
        char[] cardO = new char[4];
        char[] cardToSend = new char[4];
        System.Windows.Forms.PictureBox blueCard;
        int flag = 1;
        int flagSend = 0;
        int myScore = 0;
        int eneScore = 0;
        /*
         main function calls every thing else
         */
        private void Form1_Load(object sender, EventArgs e)
        {
            cardO[0] = '9';
            Thread myT = new Thread(talker);
            myT.Start();
            while (cardO[0] != '0')
            {

            }
            GenerateCards();
        }
        /*
         this function is the thread talking between the server and the client
         */
        private void talker()
        {
            TcpClient client = new TcpClient();
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
            client.Connect(serverEndPoint);
            NetworkStream clientStream = client.GetStream();
            clientStream.Flush();
            byte[] buffer = new byte[4096];
            int bytesRead = clientStream.Read(buffer, 0, 4096);
            for (int i = 0; i < bytesRead; i++)
                cardO[i] = Convert.ToChar(buffer[i]);
            while (true)
            {


                if (flag == 0)
                {
                    clientStream.Flush();
                    bytesRead = clientStream.Read(buffer, 0, 4096);
                    for (int i = 0; i < bytesRead; i++)
                        cardO[i] = Convert.ToChar(buffer[i]);
                    flag = 1;
                }

                while (flagSend == 0)
                {

                }
                buffer[0] = Convert.ToByte(cardToSend[0]);
                buffer[1] = Convert.ToByte(cardToSend[1]);
                buffer[2] = Convert.ToByte(cardToSend[2]);
                buffer[3] = Convert.ToByte(cardToSend[3]);
                clientStream.Write(buffer, 0, buffer.Length);
                clientStream.Flush();
                flag = 0;
                if (cardO[0] == '2')
                {
                    break;
                }

            }
            client.Close();
        }
        /*
        this function generates the board and sets all the cards
        */
        private void GenerateCards()
        {
            // blue card
            blueCard = new PictureBox();
            blueCard.Name = "blueCard";
            blueCard.Image = Properties.Resources.card_back_blue;
            blueCard.Location = new System.Drawing.Point(498, 70);
            blueCard.Size = new System.Drawing.Size(100, 114);
            blueCard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Controls.Add(blueCard);
            // red cards
            int position = 12;
            for (int i = 0; i < 10; i++)
            {

                System.Windows.Forms.PictureBox currentPic = new PictureBox();
                currentPic.Name = "pic" + i;
                currentPic.Image = Properties.Resources.card_back_red;
                currentPic.Location = new System.Drawing.Point(position, 280);
                currentPic.Size = new System.Drawing.Size(100, 114);
                currentPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                this.Controls.Add(currentPic);
                position += 110;
                currentPic.Click += delegate (object sender1, EventArgs e1)
                {
                    string card = GetCard();
                    MessageBox.Show("You've clicked card #" + card);
                    object O = Properties.Resources.ResourceManager.GetObject(card);
                    if (O == null)
                    {
                        card = "_" + card;
                        O = Properties.Resources.ResourceManager.GetObject(card);
                    }
                    ((PictureBox)sender1).Image = (Image)O;
                    this.Update();
                    flagSend = 1;


                };
            }
        }
        static Random random = new Random();
        /*
         * this function randoms a card and gets sets it 
         */
        private string GetCard()
        {
            string card = "";
            cardToSend[0] = '1';
            int num = random.Next(1, 14);
            int letter = random.Next(0, 4);
            if (num == 1)
            {
                card += "ace_";
                cardToSend[1] = '0';
                cardToSend[2] = '1';
            }
            else if (num == 11)
            {
                card += "jack_";
                cardToSend[1] = '1';
                cardToSend[2] = '1';
            }
            else if (num == 12)
            {
                card += "queen_";
                cardToSend[1] = '1';
                cardToSend[2] = '2';
            }
            else if (num == 13)
            {
                card += "king_";
                cardToSend[1] = '1';
                cardToSend[2] = '3';
            }
            else
            {
                card += num + "_";
                cardToSend[1] = '0';
                cardToSend[2] = Convert.ToChar(num);
            }
            if (letter == 0)
            {
                card += "of_spades";
                cardToSend[3] = 'S';
            }
            if (letter == 1)
            {
                card += "of_clubs";
                cardToSend[3] = 'C';
            }
            if (letter == 2)
            {
                card += "of_hearts";
                cardToSend[3] = 'H';
            }
            if (letter == 3)
            {
                card += "of_diamonds";
                cardToSend[3] = 'D';
            }
            return card;
        }
        /*
         this function changes a card to the correct image 
        */
        private void changeCard()
        {
            string card = "";
            if (cardO[2] == '1' && card[1] == '0')
            {
                card += "ace_";
            }
            else if (cardO[2] == '1' && card[1] == '1')
            {
                card += "jack_";
            }
            else if (cardO[2] == '2' && card[1] == '0')
            {
                card += "queen_";
            }
            else if (cardO[3] == '1' && card[1] == '1')
            {
                card += "king_";
            }
            else
            {
                card += "_" + cardO[2] + "_";
            }
            if (cardO[3] == 'S')
            {
                card += "of_spades";
            }
            if (cardO[3] == 'C')
            {
                card += "of_clubs";
            }
            if (cardO[3] == 'D')
            {
                card += "of_diamonds";
            }
            if (cardO[3] == 'H')
            {
                card += "of_hearts";
            }
            object O = Properties.Resources.ResourceManager.GetObject(card);
            blueCard.Image = ((Image)O);
            this.Update();
        }
        /*
         this function changes the score of the players
         */
        private void addScore()
        {
            int num1, num2;
            num1 = (int)cardToSend[1] + (int)cardToSend[2];
            num2 = (int)cardO[1] + (int)cardO[2];
            if (num1 > num2)
            {
                myScore++;
            }
            else if (num2 > num1)
            {
                eneScore++;
            }
            EneScoreL.Text = Convert.ToString(eneScore);
            myScoreL.Text = Convert.ToString(myScore);

        }

    }
}
